#include <iostream> 
#include <string>
#include <stack>
#include <algorithm>

double  add(double n1, double n2)
{   return (n1 + n2);}

double  subtract(double n1, double n2)
{   return (n1 - n2);}

double  multiply(double n1, double n2)
{   return (n1 * n2);}

double  divide(double n1, double n2)
{   return (n1 / n2);}

typedef double(*FunctionPointer)(double n1, double n2);

double getTop(std::stack<double>& nums)
{
    double temp = nums.top();
    nums.pop();
    return (temp);
}

void	stackSolve(std::stack<double>& stack, size_t op)
{
    int operand1;
    int operand2;
    FunctionPointer operators[] = {add, subtract, multiply, divide};
	operand2 = getTop(stack);
	operand1 = getTop(stack);
	stack.push(operators[op](operand1, operand2));
}

int	errMsg(std::string msg)
{
	std::cout << msg << std::endl;
	return(1);
}

int	checkArgs(int argc, char** argv)
{
	if (argc < 2 || argc > 2)
		return (errMsg("Use: RPN \"reverse polish notation expression\""));
	std::string str = argv[1];
	if (str.size() == 0)
		return (errMsg("Error: Empty experssion"));
	return (0);
}

int main(int argc, char** argv)
{
	if (checkArgs(argc, argv))
		return (1);
	std::string str = argv[1];
    size_t op;
    std::string ops = "+-*/";
	std::stack<double> stack;
	for (int i = 0; i < (int)str.size(); i+=2)
	{
		if (str[i + 1] != ' ' && (i + 1) != (int)str.size())
			return(errMsg("Error: invalid format"));
	    if (isdigit(str[i]))
		{
	        stack.push(str[i] - '0');
			continue;
		}
	    op = ops.find(str[i]);
	    if (op != ops.npos)
		{
		    if (stack.size() < 2)
				return(errMsg("Error: Not eneough operands"));
			stackSolve(stack, op);
			continue;
		}
		return(errMsg("Error: invalid character in expression"));
	}
	if (stack.size() > 1)
		return(errMsg("Error: not enough operations"));
	std::cout << stack.top() << std::endl;
	return (0);
}
