#include <fstream>
#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <iomanip>

int errMsg(std::string msg)
{
    std::cout << msg << std::endl;
    return (1);
}

int isLeapYear(int year)
{
    if (year % 4 == 0) 
    {  
        if (year % 100 == 0)
        { 
            if (year % 400 == 0)
                return (1);
        }
        else
            return (1);
    }
    return (0);
}

int checkDate(int date)
{
    int day, month, year;
    int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    year = date / 10000;
    month = (date % 10000) / 100;
    day = date % 100;
    if (year <= 0 || month <= 0 || day <= 0)
        return (1);
    if (month > 12)
        return (1);
    if (day > days[month] + ((month == 2)?isLeapYear(year):0))
        return (1);
    return (0);
}

int readDate(std::string line, std::string& dateStr)
{
    int date = 0;
    for (int i = 0; i < 10; i++)
    {
        if ((i != 4 && i != 7) && isdigit(line[i]))
            date = date * 10 + line[i] - '0';
        if ((i == 4 || i == 7) && line[i] != '-')
            return (errMsg("Error: bad input => " + line));
    }
    if (checkDate(date))
        return (errMsg("Error: bad input => " + line));
    dateStr = line.substr(0, 10);
    return (0);
}

int readValue(std::string line, double& value)
{
    size_t   i;
    std::string             valueStr;

    i = line.find('|');
    if (i == line.npos)
        return (errMsg("Error: bad format"));
    valueStr = line.substr(i + 1, line.size() - i - 1);
    if (valueStr.size() == 0)
        return (errMsg("Error: value not found"));
    value = atof(&valueStr[0]);
    if (value < 0)
        return (errMsg("Error: not a positive number"));
    if (value >= 1000)
        return (errMsg("Error: too large a number"));
    return (0);
}

int processExchange(std::map<std::string, double> exchangeRates, std::string date, double value)
{
    if (exchangeRates.begin()->first.compare(date) > 0)
        return (errMsg("Error: exchange date predates database"));
    if ((--exchangeRates.end())->first.compare(date) <= 0)
        std::cout << date << " => " << value << " = " << value * (--exchangeRates.end())->second << std::endl;
    else
        std::cout << date << " => " << value << " = " << value * (--exchangeRates.upper_bound(date))->second << std::endl;
    return (0);
}

int processInput(std::string dataFile, std::map<std::string, double> &exchangeRates)
{
    std::string     date;
    double          value;
    std::ifstream   data;
    std::string     line;
    
    data.open(&dataFile[0]);
    if (data.fail())
        return (errMsg("fail to open file input"));
    std::getline(data,line);
    while (!data.eof())
    {
        std::getline(data,line);
        if (line.size() == 0)
            continue;
        line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
        if (readDate(line, date))
            continue;
        if (readValue(line, value))
            continue;
        processExchange(exchangeRates, date, value);
    }
    data.close();
    return (0);
}

int getData(std::string dataFile, std::map<std::string, double> &exchangeRates)
{
    std::fstream   data;
    std::string     line;

    data.open(&dataFile[0]);
    if (data.fail())
        return (errMsg("fail to open file database"));
    while (!data.eof())
    {
        std::getline(data,line);
        if (line.size() == 0 || !isdigit(line[0]))
            continue;
        exchangeRates[line.substr(0, 10)] = strtod(&line[11], NULL);
    }
    data.close();
    return (0);
}

int main(int argc, char **argv)
{
    std::map<std::string, double>    exchangeRates;
    if (argc < 2 || argc > 2)
        return (errMsg("Use: BTC <inputfile.txt>"));
    if (getData("data.csv", exchangeRates))
        return (1);
    if (processInput(argv[1], exchangeRates))
        return (1);
    return (0);
}
