#include "PmergeMe.hpp"


int ft_binarySearchV(std::vector<int> * con, int value)
{
    int start = 0;
    int end = con->size() - 1;
    int middle;
    while (1)
    {
        middle = (end - start) / 2 + start;
        if (value > con->at(middle))
            end = middle;
        else
            start = middle;
        if (end - 1 == start)
            break;
    }
    return (start);
}

std::vector<int>* getInsertionOrderV(int size)
{
    std::vector<int> *order = new std::vector<int>;
    order->push_back(0);
    order->push_back(1);
    int jtsLast = 1;
    int jtscurrent = 1;
    int jtsNext = 0;
    int counter = 2;
    while (counter < size)
    {
        jtsNext = jtscurrent + (jtsLast * 2);
        jtsLast = jtscurrent;
        jtscurrent = jtsNext;
        while (jtsNext > jtsLast)
        {
            if (jtsNext >= size)
                jtsNext = size - 1;
            order->push_back(jtsNext);
            counter++;
            jtsNext--;
        }
    }
    return (order);
}

std::vector<std::pair<int, int> >  makeVcPairs(std::vector<int> data)
{
    std::vector<std::pair<int, int> > pairs;
    for (std::vector<int>::iterator i = data.begin(); i != data.end(); i++)
    {
        if (i + 1 == data.end())
            pairs.push_back(std::pair<int, int>(-1 , *i));
        else
        {
            pairs.push_back(std::pair<int, int>(*i, *(i + 1)));
            i++;
        }
        if (pairs.back().first < pairs.back().second)
            swapInt(pairs.back().first, pairs.back().second);
    }
    return (pairs);
}

void vcPairSort(std::vector<std::pair<int, int> >& pairs)
{
    if (pairs.size() == 1)
        return;
    std::vector<std::pair<int, int> > half1(pairs.begin(), pairs.begin() + pairs.size()/2);
    std::vector<std::pair<int, int> > half2(pairs.begin() + pairs.size()/2, pairs.end());
    vcPairSort(half1);
    vcPairSort(half2);
    size_t i = 0;
    size_t j = 0;
    pairs.clear();
    while (i < half1.size() || j < half2.size())
    {
        if (j == half2.size())
        {
            pairs.push_back(half1[i++]);
        }
        else if (i == half1.size())
        {
            pairs.push_back(half2[j++]);
        }
        else if (half1[i].first > half2[j].first)
        {
            pairs.push_back(half1[i++]);
        }
        else if (half1[i].first <= half2[j].first)
        {
            pairs.push_back(half2[j++]);
        }
    }    
}

std::vector<int>* vcSort(int argc, char** argv)
{
    std::vector<int>* insertionOrder;
    std::vector<int>* data = new std::vector<int>;
    for (int i = 1; i < argc; i++)
        data->push_back(std::atoi(argv[i]));
    std::vector<std::pair<int, int> > pairs = makeVcPairs(*data);
    data->clear();
    vcPairSort(pairs);
    insertionOrder = getInsertionOrderV(pairs.size());
    for (size_t i = 0; i < pairs.size(); i++)
        data->push_back(pairs[i].first);
    for (std::vector<int>::iterator i = insertionOrder->begin(); i != insertionOrder->end(); i++)
    {
        if (pairs[*i].second >= 0)
        {
            if (pairs[*i].second >=  data->front())
                data->insert(data->begin(), pairs[*i].second);
            else if (pairs[*i].second <=  data->back())
                data->push_back(pairs[*i].second);
            else
                data->insert(data->begin() + ft_binarySearchV(data, pairs[*i].second) + 1, pairs[*i].second);
        }
    }
    delete(insertionOrder);
    return(data);
}
