#ifndef PMERGEME_HPP
# define PMERGEME_HPP

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <utility>
#include <deque>
#include <vector>
#include <iomanip>

bool    ft_isPositiveInt(char* str);
int     errMsg(std::string msg);
void    swapInt(int& n1, int& n2);

std::deque<int> *dqSort(int argc, char** argv);
std::vector<int> *vcSort(int argc, char** argv);

#endif