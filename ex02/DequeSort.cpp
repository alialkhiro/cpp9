#include "PmergeMe.hpp"

int ft_binarySearchD(std::deque<int> * con, int value)
{
    int start = 0;
    int end = con->size() - 1;
    int middle;
    while (1)
    {
        middle = (end - start) / 2 + start;
        if (value > con->at(middle))
            end = middle;
        else
            start = middle;
        if (end - 1 == start)
            break;
    }
    return (start);
}

std::deque<int>* getInsertionOrder(int size)
{
    std::deque<int> *order = new std::deque<int>;
    order->push_back(0);
    order->push_back(1);
    int jtsLast = 1;
    int jtscurrent = 1;
    int jtsNext = 0;
    int counter = 2;
    while (counter < size)
    {
        jtsNext = jtscurrent + (jtsLast * 2);
        jtsLast = jtscurrent;
        jtscurrent = jtsNext;
        while (jtsNext > jtsLast)
        {
            if (jtsNext >= size)
                jtsNext = size - 1;
            order->push_back(jtsNext);
            counter++;
            jtsNext--;
        }
    }
    return (order);
}

std::deque<std::pair<int, int> >  makeDQPairs(std::deque<int> data)
{
    std::deque<std::pair<int, int> > pairs;
    for (std::deque<int>::iterator i = data.begin(); i != data.end(); i++)
    {
        if (i + 1 == data.end())
            pairs.push_back(std::pair<int, int>(-1 , *i));
        else
        {
            pairs.push_back(std::pair<int, int>(*i, *(i + 1)));
            i++;
        }
        if (pairs.back().first < pairs.back().second)
            swapInt(pairs.back().first, pairs.back().second);
    }
    return (pairs);
}

void dqPairSort(std::deque<std::pair<int, int> >& pairs)
{
    if (pairs.size() == 1)
        return;
    std::deque<std::pair<int, int> > half1(pairs.begin(), pairs.begin() + pairs.size()/2);
    std::deque<std::pair<int, int> > half2(pairs.begin() + pairs.size()/2, pairs.end());
    dqPairSort(half1);
    dqPairSort(half2);
    size_t i = 0;
    size_t j = 0;
    pairs.clear();
    while (i < half1.size() || j < half2.size())
    {
        if (j == half2.size())
        {
            pairs.push_back(half1[i++]);
        }
        else if (i == half1.size())
        {
            pairs.push_back(half2[j++]);
        }
        else if (half1[i].first > half2[j].first)
        {
            pairs.push_back(half1[i++]);
        }
        else if (half1[i].first <= half2[j].first)
        {
            pairs.push_back(half2[j++]);
        }
    }    
}

std::deque<int> *dqSort(int argc, char** argv)
{
    std::deque<int>* insertionOrder;
    std::deque<int>* data = new std::deque<int>;
    for (int i = 1; i < argc; i++)
        data->push_back(std::atoi(argv[i]));
    std::deque<std::pair<int, int> > pairs = makeDQPairs(*data);
    data->clear();
    dqPairSort(pairs);
    insertionOrder = getInsertionOrder(pairs.size());
    for (size_t i = 0; i < pairs.size(); i++)
        data->push_back(pairs[i].first);
    for (std::deque<int>::iterator i = insertionOrder->begin(); i != insertionOrder->end(); i++)
    {
        if (pairs[*i].second >= 0)
        {
            if (pairs[*i].second >=  data->front())
                data->push_front(pairs[*i].second);
            else if (pairs[*i].second <=  data->back())
                data->push_back(pairs[*i].second);
            else
                data->insert(data->begin() + ft_binarySearchD(data, pairs[*i].second) + 1, pairs[*i].second);
        }
    }
    delete(insertionOrder);
    return(data);
}
