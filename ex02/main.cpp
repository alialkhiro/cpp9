#include "PmergeMe.hpp"

bool    ft_isPositiveInt(char* str)
{
    if (*str == '\0')
        return (false);
    while (*str)
    {
        if (!isdigit(*str))
            return (false);
        str++;
    }
    return (true);
}

int errMsg(std::string msg)
{
    std::cout << msg << std::endl;
    return (1);
}

void    swapInt(int& n1, int& n2)
{
    int temp = n1;
    n1 = n2;
    n2 = temp;
}

void simpleSort(std::vector<int>& args)
{
    if (args.size() == 1)
        return;
    std::vector<int> half1(args.begin(), args.begin() + args.size()/2);
    std::vector<int> half2(args.begin() + args.size()/2, args.end());
    simpleSort(half1);
    simpleSort(half2);
    size_t i = 0;
    size_t j = 0;
    args.clear();
    while (i < half1.size() || j < half2.size())
    {
        if (j == half2.size())
        {
            args.push_back(half1[i++]);
        }
        else if (i == half1.size())
        {
            args.push_back(half2[j++]);
        }
        else if (half1[i] > half2[j])
        {
            args.push_back(half1[i++]);
        }
        else if (half1[i] <= half2[j])
        {
            args.push_back(half2[j++]);
        }
    }    
}

void    printArgs(int argc, char **args)
{
    for (int i = 1; i < argc; i++)
    {
        std::cout << " " << *args;
        args++;
    }
    std::cout << std::endl;
}

void printResults(std::vector<int> results)
{
    for (std::vector<int>::reverse_iterator i = results.rbegin(); i != results.rend(); i++)
        std::cout << " " << *i;
    std::cout << std::endl;
}

int compareResultsV(std::vector<int> results, std::vector<int>* con)
{
    int j = 0;
    if (results.size() != con->size())
        return (errMsg("size mismatch"));
    for (std::vector<int>::iterator i = con->begin(); i != con->end(); i++)
    {
        if (results[j++] != *i)
            return (errMsg("value mismatch"));;
    }
    return (0);
}

int compareResultsD(std::vector<int> results, std::deque<int>* con)
{
    int j = 0;
    if (results.size() != con->size())
        return (errMsg("size mismatch"));
    for (std::deque<int>::iterator i = con->begin(); i != con->end(); i++)
    {
        if (results[j++] != *i)
            return (errMsg("value mismatch"));;
    }
    return (0);
}

int main(int argc, char** argv)
{
    std::vector<int> args;
    std::vector<int>* resultsVector;
    std::deque<int>* resultsDeque;
    for (int i = 1; i < argc; i++)
        if (!ft_isPositiveInt(argv[i]))
            return (errMsg("invalid argument"));
    for (int i = 1; i < argc; i++)
        args.push_back(std::atoi(argv[i]));
    std::cout << "Before: ";
    printArgs(argc, argv);
    std::cout << "After: ";
    simpleSort(args);
    printResults(args);
    double startT, dequeT, vectorT;
    startT = static_cast<double>(std::clock()) / CLOCKS_PER_SEC;
    resultsDeque = dqSort(argc, argv);
    dequeT = static_cast<double>(std::clock() - startT) / CLOCKS_PER_SEC;;
    resultsVector = vcSort(argc, argv);
    vectorT = static_cast<double>(std::clock() - dequeT) / CLOCKS_PER_SEC;;
    if (compareResultsD(args, resultsDeque) || compareResultsV(args, resultsVector))
    {
        delete(resultsDeque);
        delete(resultsVector);
        return (errMsg("sorting failure"));
    }
    delete(resultsDeque);
    delete(resultsVector);
    std::cout << "Time to process a range of " << argc - 1 << " elements with std::deque  : " << std::setprecision(5) << dequeT << " us" << std::endl;
    std::cout << "Time to process a range of " << argc - 1 << " elements with std::vector : " << std::setprecision(5) << vectorT << " us" << std::endl;
    return (0);
}
